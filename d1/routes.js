const http = require("http");

// storing the 400 in  a variable called port
const port = 4000;

// storing the createServer method inside the server variable
const server = http.createServer((request,response)=>{

    /* 
    "request" is an object that is sent via the client (browser)
    "url" is the property of the request that refers to the endpoint of the link
    */

    // the condition below means that we have access to the localhost: 4000 with "/greeting" in its endpoint
    if (request.url === "/greeting"){
        response.writeHead(200,{"Content-Typoe":"text/plain"});
        response.end("Hello World");
    } else if (request.url === "/homepage"){
        response.writeHead(200,{"Content-Typoe":"text/plain"});
        response.end("Hello");
    } else {
        response.writeHead(404,{"Content-Type":"text/plain"});
        response.end("Page not found");
    }



});




server.listen(port);

console.log(`Server now running at port: ${port}`);