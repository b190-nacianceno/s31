/* 
We use "require" directive to load Node.js modules
-A modules is a software component or a part of a program that containes one or more routines.

"http" module lets Node.js transfer data using Hyper Text Transfer Protocol

"http" module is a set of individual files that contain code to create a "component" that helps establish data transfer between applications

HTTP is a protocol that allow sthe fetching of resources such as HTML documents

Clients (browser) server (Node.js/expressJS applications) communicate by exchanging individual messages 

message that come from the clients- request
messages that come from the server - response
*/

let http = require("http");

/* 
http- we are trying to use the http module for us  to create our server-side application

createServer() - found inside the http module; a mehtod that accepts a function as its  argument for a creation of a server

(request,response) - arguments that are passed to the createServer method; this would allow us to receive requests (1st parameter) and send responses (2nd parameter)

writeHead() - to set a status code for the response - 200 means success/OK status (search different HTTP status code: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

Content-Type - using "text/plain" means that we are send plain text as a response

response.end - to denote the last stage of communication which is the sending of the response from the server - code below will send the response: "Hello World"

.listen() allows te applcation to run in our local devices through a specified port; a port is a virtual point where network connection start and end. Each port is associated with a specific process/service

the code below means that the server will be assigned to port 4000.The server will listen to any requests that are sent to it eventually communicating with our server
*/
http.createServer(function(request,response){
    response.writeHead(200,{"Content-Type": "text/plain"});
    response.end("Hello world");
}).listen(4000);


/* 
use 'node index.js' on the terminal to run the server
press ctrl + c to terminate the gitbash process
*/

// used to confirm if the server is running on a port
console.log("Server running at port: 4000");


/* 
nodemon 

- installing this package will allow the server to automaticaly restart when files have been changed for update i.e. saving the files
-npm install means that we are going to access the npm and install one fo the packages that is in its library
   sudo npm install -g nodemon
   -"-g" means that we are installing the package globally in our device. This means that even if we are in other directories/repositories in our device, we could still use nodemon. 
*/